//
//  MainTableViewCell.swift
//  DesafioConcreteSwift23
//
//  Created by Richard Frank on 21/11/16.
//  Copyright © 2016 Richard Frank. All rights reserved.
//

import UIKit

class MainTableViewCell: UITableViewCell {

    //MARK: - IBOutlets
    
    /// IBOutlet Repository
    @IBOutlet weak var labelNameRepo: UILabel!
    @IBOutlet weak var labelDescriptionRepo: UILabel!
    @IBOutlet weak var labelStarsRepo: UILabel!
    @IBOutlet weak var labelForksRepo: UILabel!
    
    /// IBOutlet Author
    @IBOutlet weak var imageAvatarOwner: UIImageView!
    @IBOutlet weak var labelLoginOwner: UILabel!
}
